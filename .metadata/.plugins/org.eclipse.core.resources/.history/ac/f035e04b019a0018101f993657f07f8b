
/**
  ******************************************************************************
  * @file           : SIRC_Decode.c
  * @brief          : SIRC decode functions
  * @author			: Nhuan_HD
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f0xx_hal.h"

/** @defgroup Global_Public_Variables
  * @{
  */
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;

UART_HandleTypeDef huart1;

/**
  * @}
  */

/** @defgroup SIRC_Private_Constants
  * @{
  */
#define 	RISING_EDGE 			1
#define 	FALLING_EDGE 			0
#define		TIMEOUT					35
#define		SUCCESS					1
#define		FAIL					0
#define 	IC_PRESCALE				50

/*Display macros*/
#define	DISPLAY_NUMBER(huart, string, number) 	HAL_UART_Transmit(huart, (uint8_t*)BUFFER_Display, sprintf(BUFFER_Display, string, number), 500)
#define	DISPLAY_STRING(huart, string, size) 	HAL_UART_Transmit(huart, (uint8_t*)string, size, 500)

/*Decode state*/
enum
{
	INIT,
	IDLE,
	RECEIVING,
	RECEIVE_DONE
};

/**
  * @}
  */

/** @defgroup SIRC_Private_Variables
  * @{
  */

/*Input capture definitions*/
volatile 	uint32_t 	IC_Falling = 0;
volatile 	uint32_t 	IC_Rising = 0;
volatile 	uint32_t 	IC_CaptureEdge = 0;

/*SIRC decode definitions*/
volatile	uint16_t  	aSIRC_CaptureValue[100];
volatile 	uint8_t	  	SIRC_DecodeState = INIT;
volatile 	uint16_t  	SIRC_CounterData = 0;
volatile	uint8_t	  	SIRC_DataLength = 0;
volatile	uint8_t	  	SIRC_Repeat = 0;

/*Time out counter*/
volatile	uint16_t  	TIMOUT_Counter = 0;

uint16_t 	BUFFER_Display[100];

/**
  * @}
  */

/** @defgroup SIRC_Publich_Variables
  * @{
  */
__IO  char	SIRC_DataReady = 0;


/** @defgroup SIRC_Private_Function_Prototypes
  * @{
  */

static void MX_GPIO_Init(void);
static void MX_TIM2_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_TIM3_Init(void);

void SIRC_Decode();
void SIRC_EnableRecive();
void SIRC_Init();
void SystemClock_Config(void);


/**
  * @}
  */

/** @defgroup SIRC_Private_Functions
  * @{
  */
/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* TIM2 init function */
static void MX_TIM2_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_IC_InitTypeDef sConfigIC;

  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 8*IC_PRESCALE;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 0xFFFF;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  if (HAL_TIM_IC_Init(&htim2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_BOTHEDGE;
  sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
  sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
  sConfigIC.ICFilter = 0;
  if (HAL_TIM_IC_ConfigChannel(&htim2, &sConfigIC, TIM_CHANNEL_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
}

/* TIM3 init function */
static void MX_TIM3_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 8;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 1000;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART1 init function */
static void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** Configure pins as
        * Analog
        * Input
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
}

/**
  * @brief  SIRC initialize
  * @param  None
  * @retval None
  */
void SIRC_Init()
{
	HAL_Init();
    SystemClock_Config();
    MX_GPIO_Init();
    MX_TIM2_Init();
    MX_USART1_UART_Init();
  	MX_TIM3_Init();
  	HAL_TIM_IC_Start_IT(&htim2, TIM_CHANNEL_1);
	SIRC_DecodeState = IDLE;
}

/**
  * @brief  SIRC enable receive
  * @param  None
  * @retval None
  */
void SIRC_EnableReceive()
{
	SIRC_DataReady = 0;
	SIRC_Repeat = 0;
	SIRC_CounterData = 0;
	SIRC_DecodeState = IDLE;
	HAL_TIM_IC_Start_IT(&htim2, TIM_CHANNEL_1);
}

/**
  * @brief  SIRC Decode
  * @param  None
  * @retval None
  */
void SIRC_Decode()
{
	uint16_t 	SIRC_Data = 0;
	uint8_t  	index;
	uint8_t	  	SIRC_CheckData = 0;
	uint8_t		SIRC_WrongIndex = 0;

	/*Display raw data*/
	DISPLAY_STRING(&huart1, "[SIRC capture values]\n", 22);
	DISPLAY_STRING(&huart1, "\n", 1);
	for(index = 0; index < 7; index++)
	{
		DISPLAY_NUMBER(&huart1, "%d - ", index);
		DISPLAY_NUMBER(&huart1, "%d     ", aSIRC_CaptureValue[index]*IC_PRESCALE);
	}

	DISPLAY_STRING(&huart1, "\n", 1);
	for(index = 7; index < 13; index++)
	{
		DISPLAY_NUMBER(&huart1, "%d - ", index);
		DISPLAY_NUMBER(&huart1, "%d     ", aSIRC_CaptureValue[index]*IC_PRESCALE);
	}

	DISPLAY_STRING(&huart1, "\n", 1);
	/*Decode HEADER*/
	if(aSIRC_CaptureValue[0]*IC_PRESCALE >= 2150 && aSIRC_CaptureValue[0]*IC_PRESCALE <= 2650)
	{
		/*HEADER OK*/
		SIRC_Data = 0;
		for(index = 1; index < 13; index++)
		{
		  /*Check bit 0*/
		  if (aSIRC_CaptureValue[index]*IC_PRESCALE >= 450 && aSIRC_CaptureValue[index]*IC_PRESCALE <= 750)
			  SIRC_Data = SIRC_Data << 1;

		  /*Check bit 1*/
		  else if (aSIRC_CaptureValue[index]*IC_PRESCALE >= 1050 && aSIRC_CaptureValue[index]*IC_PRESCALE <= 1350)
		  {
			  SIRC_Data = (SIRC_Data << 1)|1;
			  SIRC_CheckData = SUCCESS;
		  }

		  /*Data unknown*/
		  else
		  {
			  SIRC_WrongIndex = index;
			  SIRC_CheckData = FAIL;
			  break;
		  }
		}

		if (SIRC_CheckData == SUCCESS)
		{
			uint16_t i = 0;
			uint16_t j = 0;
			uint16_t Command = 0;
			uint16_t Address = 0;

			/*Reverse 16 bit data*/
			for (j = i = 0; i < 16; i++)
			{
			  j = (j << 1) + (SIRC_Data & 1);
			  SIRC_Data >>= 1;
			}
			/*Get Command value*/
			Command = (j >> 4) & 0x7F;
			/*Get Address value*/
			Address = (j >> 11) & 0x1F;
			DISPLAY_STRING(&huart1, "\n", 1);
			DISPLAY_NUMBER(&huart1, "------>>>> Command: %d - ", Command);
			DISPLAY_NUMBER(&huart1, ": %d - ", Address);
			/*DISPLAY_NUMBER(&huart1, "Repeat: %d\n", SIRC_Repeat);*/
			DISPLAY_STRING(&huart1, "\n", 1);
		}
		else
		{
			DISPLAY_NUMBER(&huart1, "----->>>> %d out of range!!!\n", SIRC_WrongIndex);
		}
	}
	else
	{
		/*HEADER unknown*/
		DISPLAY_STRING(&huart1, "----->>>> HEADER INCORRECT!!!\n", 30);
	}
}

/**
  * @}
  */

/**
  * @}
  */

/*****************************END OF FILE****/
























