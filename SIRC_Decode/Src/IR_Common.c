/*
 * IR_Common.c
 *
 *  Created on: Aug 10, 2018
 *      Author: Admin
 */

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"
#include "IR_Common.h"

/** @defgroup SIRC_Private_Constants
  * @{
  */
#define 	RISING_EDGE 			1
#define 	FALLING_EDGE 			0
#define		TIMEOUT					35

/** @defgroup SIRC_Peripheral
  * @{
  */
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim16;
TIM_HandleTypeDef htim17;
UART_HandleTypeDef huart1;

/*SIRC decode definitions*/
volatile 	uint8_t	  	SIRC_DecodeState = INIT;
volatile	uint16_t  	aSIRC_CaptureValue[100];
volatile 	uint16_t  	SIRC_CounterData = 0;
volatile	uint8_t	  	SIRC_Repeat = 0;
volatile	uint8_t	  	SIRC_DataReady = 0;

/*Time out counter*/
volatile	uint16_t  	TIMOUT_Counter = 0;

/*Input capture definitions*/
volatile 	uint32_t 	IC_Falling = 0;
volatile 	uint32_t 	IC_Rising = 0;
volatile 	uint32_t 	IC_CaptureEdge = 0;
/**
  * @brief  Input capture interrupt
  * 		This function's called when program detect both falling edge and	rising edge
  * @param  None
  * @retval None
  */
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1)
	{
		/*Enable TIMER 1*/
		if(SIRC_DecodeState == IDLE)
		{
			HAL_TIM_Base_Start_IT(&htim3);
			SIRC_DecodeState = RECEIVING;
		}
		TIMOUT_Counter = 0;

		if (IC_CaptureEdge == FALLING_EDGE)
		{
			IC_Falling = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1);
			IC_CaptureEdge = RISING_EDGE;
		}
		else
		{
			IC_Rising = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1);
			if (IC_Falling > IC_Rising)
			{
				aSIRC_CaptureValue[SIRC_CounterData] = (0xFFFF - IC_Falling) + IC_Rising + 1;
				SIRC_CounterData++;
			}
			else
			{
				aSIRC_CaptureValue[SIRC_CounterData] = IC_Rising - IC_Falling;
				SIRC_CounterData++;
			}
			IC_CaptureEdge = FALLING_EDGE;
			if(SIRC_CounterData > 13)
			{
				SIRC_Repeat++;
			}
		}
	}
}

/**
  * @brief  Timer 3 for Time our overflow interrupt
  * 		This function's called when out of time
  * @param  None
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim == &htim3)
	{
		if(SIRC_DecodeState == RECEIVING)
		{
			TIMOUT_Counter++;
			if(TIMOUT_Counter > TIMEOUT)
			{
				HAL_TIM_Base_Stop_IT(htim);
				HAL_TIM_Base_Stop_IT(&htim2);

				SIRC_DataReady = 1;
				SIRC_DecodeState == RECEIVE_DONE;
			}
		}
	}
	if(htim == &htim16)
	{
		SIRC_Encode_SignalGenerate();
	}
}
/**
  * @brief  Force new configuration to the output channel
  * @param  action: new configuration
  * @retval None
  */
void TIM_ForcedOC1Config(uint32_t action)
{
  uint32_t temporary = htim16.Instance->CCMR1;

  temporary &= ~TIM_CCMR1_OC1M;
  temporary |= action;
  htim16.Instance->CCMR1 = temporary;
}

