/**
  ******************************************************************************
  * @file    sirc_encode.c
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    08-March-2016
  * @brief   This file provides all the sirc encode firmware functions
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "sirc_encode.h"


/** @addtogroup SIRC_ENCODE
  * @brief SIRC_ENCODE driver module
  * @{
  */

/**
  * @addtogroup SIRC_Private_Defines
  * @{
  */
#define  SIRC_HIGH_STATE_NB_SYMBOL     ((uint8_t)3)        /* Nb high state symbol definition*/
#define  SIRC_LOW_STATE_NB_SYMBOL      ((uint8_t)2)        /* Nb low state symbol definition*/
#define  SIRC_ADDRESS_BITS             ((uint8_t)5)        /* Nb of data bits definition*/
#define  SIRC_INSTRUCTION_BITS         ((uint8_t)7)        /* Nb of data bits definition*/
#define  SIRC_HIGH_STATE_CODE          ((uint8_t)0x03)     /* SIRC high level definition*/
#define  SIRC_LOW_STATE_CODE           ((uint8_t)0x01)     /* SIRC low level definition*/
#define  SIRC_MAIN_FRAME_LENGTH        ((uint8_t)12)       /* Main frame length*/
#define  SIRC_BIT_FORMAT_MASK          ((uint16_t)1)       /* Bit mask definition*/
#define  SIRC_COUNTER_LIMIT            ((uint16_t)75)      /* main frame + idle time */
#define  SIRC_IS_RUNNING               ((uint8_t)4)        /* SIRC Protocol number */
#define  SIRC_HEADERS                  ((uint16_t)0x0F)    /* SIRC Start pulse */
#define  SIRC_HEADERS_LENGTH           ((uint8_t)5)        /* Length of the headers */
#define  SIRC_CODED_FRAME_TABLE_LENGTH ((uint8_t)2)        /* Coded frame table number of uint32_t word  */
#define TIM_FORCED_ACTIVE      ((uint16_t)0x0050)
#define TIM_FORCED_INACTIVE    ((uint16_t)0x0040)

TIM_HandleTypeDef htim16;
TIM_HandleTypeDef htim17;
/**
  * @}
  */

/**
  * @addtogroup SIRC_Private_Function_Protoypes
  * @{
  */
static void SIRC_PulseWidthModulationConvert(uint32_t bindata, uint8_t bindatalength);
static void SIRC_Shift_Table(__IO uint32_t aTable[]);
static void SIRC_AddHeaders(uint8_t headers);
static void SIRC_AddStateFragment(uint8_t State, uint8_t freespace);
static void SIRC_AddHeadersFragment(uint8_t headers, uint8_t freespace);
static uint32_t SIRC_MSBToLSB_Data(uint32_t Data, uint8_t DataNbBits);
static uint16_t SIRC_BinFrameGeneration(uint8_t SIRC_Address, uint8_t SIRC_Instruction);
static void SIRC_Encode_DeInit(void);
void TIM_ForcedOC1Config(uint32_t action);
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);
/**
  * @}
  */

/**
  * @addtogroup SIRC_Public_Variables
  * @{
  */
__IO uint32_t aSIRCFramePWForm[SIRC_CODED_FRAME_TABLE_LENGTH]; //Khai bao mang 2 phan tu

/**
  * @}
  */

/**
  * @addtogroup SIRC_Private_Variables
  * @{
  */
__IO uint16_t SIRCFrameBinaryFormat = 0;
uint8_t SIRCSendOpReadyFlag = RESET;
uint8_t SIRCSendOpCompleteFlag = SET;
uint8_t SIRCBitsSentCounter = 0;
uint8_t SIRCCodedFrameLastWordLength = 0;
uint8_t SIRCNbWord = 0;

/**
  * @brief Generate and Send the SIRC frame.
  * @param SIRC_Address : the SIRC Device destination
  * @param SIRC_Instruction : the SIRC command instruction
  * @retval  None
  */
void SIRC_Encode_SendFrame(uint8_t SIRC_Address, uint8_t SIRC_Instruction)
{
  /* Check the parameters */
  assert_param(IS_SIRC_ADDRESS_IN_RANGE(SIRC_Address));
  assert_param(IS_SIRC_INSTRUCTION_IN_RANGE(SIRC_Instruction));

  /* Generate a binary format of the message */
  SIRCFrameBinaryFormat = SIRC_BinFrameGeneration(SIRC_Address, SIRC_Instruction);

  /* Transform address and data from MSB first to LSB first */
  SIRCFrameBinaryFormat = SIRC_MSBToLSB_Data(SIRCFrameBinaryFormat, SIRC_MAIN_FRAME_LENGTH);

  /* Convert the frame binary format to a PulseWidthModulation format of the message */
  SIRC_PulseWidthModulationConvert(SIRCFrameBinaryFormat, SIRC_MAIN_FRAME_LENGTH);

  /* Add the headers to SIRC_FramePulseWidthFormat Table */
  SIRC_AddHeaders(SIRC_HEADERS);

  /* Set the Send operation Ready flag to indicate that the frame is ready to be sent */
  SIRCSendOpReadyFlag = SET;

  /* Reset the counter to ensure accurate timing */
  __HAL_TIM_SET_COUNTER(&htim16, 0);

  /* TIM IT Enable */
  HAL_TIM_Base_Start_IT(&htim16);
}

/**
  * @brief Send by hardware PulseWidthModulation Format SIRC Frame.
  * @retval none
  */
void SIRC_Encode_SignalGenerate(void)
{
  uint32_t tablecounter = 0;
  uint32_t bitmsg = 0;

  if ((SIRCSendOpReadyFlag != RESET) && (SIRCBitsSentCounter <= ( SIRC_COUNTER_LIMIT ))) //75
  {
    /*Reset send operation complete flag*/
    SIRCSendOpCompleteFlag = RESET;

    /*Read message bits*/
    if (SIRCBitsSentCounter < SIRCCodedFrameLastWordLength)
    {
      /*Read coded frame bits from the last word*/
      bitmsg = (uint8_t)((aSIRCFramePWForm[0] >> SIRCBitsSentCounter) & 1); //Lay tung bit
    }
    else
    {
      /*Read coded frame bits from the table*/
      bitmsg = (uint8_t)((aSIRCFramePWForm[((SIRCBitsSentCounter-SIRCCodedFrameLastWordLength)/32)+1] >> (SIRCBitsSentCounter - SIRCCodedFrameLastWordLength)) & 1);
    }

    /* Generate signal */
    if (bitmsg != RESET)
    {
      TIM_ForcedOC1Config(TIM_FORCED_ACTIVE);
    }
    else if (bitmsg == RESET)
    {
      TIM_ForcedOC1Config(TIM_FORCED_INACTIVE);
    }
    else if (SIRCBitsSentCounter <= ( SIRCNbWord*32 + SIRCCodedFrameLastWordLength ))
    {
      TIM_ForcedOC1Config(TIM_FORCED_INACTIVE);
    }

    SIRCBitsSentCounter++;
  }
  else /* Sending complete */
  {
    /* Reset flags   */
    SIRCSendOpCompleteFlag = SET;
    SIRCSendOpReadyFlag = RESET;
    /* TIM IT Disable */
    HAL_TIM_Base_Stop_IT(&htim16);
    /*Reset counters */
    SIRCNbWord = 0;
    SIRCBitsSentCounter = 0;
    /*Reset counters */
    SIRCCodedFrameLastWordLength = 0;
    SIRCFrameBinaryFormat = 0;

    /*Reset frame temporary variables*/
    for (tablecounter = 0; tablecounter < SIRCNbWord; tablecounter++)
    {
      aSIRCFramePWForm[tablecounter] = 0x0;
    }
    TIM_ForcedOC1Config(TIM_FORCED_INACTIVE);

    /* TIM Disable */
    __HAL_TIM_DISABLE(&htim16);
  }
}

/**
  * @}
  */

/**
  * @addtogroup SIRC_Private_Functions
  * @{
  */

/**
  * @ Transform the frame binary form from MSB to LSB.
  * @param Data: Frame binary format to inverse
  * @param DataNbBits: size of the transmission
  * This parameter can be any of the @ref uint32_t.
  * @retval Symmetric binary frame form
  */
static uint32_t SIRC_MSBToLSB_Data(uint32_t Data , uint8_t DataNbBits)
{
  uint32_t temp = 0;     /* Temporary variable to memorize the converted message */
  uint8_t datacount = 0; /* Counter of bits converted */

  /* Shift the temporary variable to the left and add one bit from the Binary frame  */
  for (datacount = 0; datacount < (DataNbBits); datacount++)
  {
    temp = temp << 1;
    temp |= ((Data >> datacount) & 1);
  }
  return temp;
}

/**
  * @brief Generate the binary format of the SIRC frame.
  * @param SIRC_Address : Select the device address
  * @param SIRC_Instruction : Select the device instruction
  * @retval Binary format of the SIRC Frame.
  */
static uint16_t SIRC_BinFrameGeneration(uint8_t SIRC_Address, uint8_t SIRC_Instruction)
{
  /* wait until the ongoing Frame sending operation finishes */
  while (SIRCSendOpCompleteFlag == RESET)
  {}

  /* Reset SIRC_Send_Operation_Ready_f flag to mention that a send operation can be treated */
  SIRCSendOpReadyFlag = RESET;

  /* Concatenate Binary Frame Format */
  SIRCFrameBinaryFormat = (SIRC_Address << SIRC_INSTRUCTION_BITS ); // Shift left 7 bit
  SIRCFrameBinaryFormat = SIRCFrameBinaryFormat | (SIRC_Instruction);

  return (SIRCFrameBinaryFormat);
}

/**
  * @Shift the coded frame table by one box.
  * @param aTable: coded data table
  * This parameter can be any of the @ref uint32_t.
  * @retval  None
  */
static void SIRC_Shift_Table(__IO uint32_t aTable[])
{
  uint8_t i = 0;

  /* Increment the coded frame table words number */
  SIRCNbWord++;

  /* Shift the coded frame table to the left by one box */
  for (i = 0; i < SIRCNbWord; i++)
  {
    aTable[SIRCNbWord-i] = aTable[SIRCNbWord - i - 1];
  }
  /* Clear the first the coded frame table box */
  aTable[0] = 0;

  /* Reset the last word length counter */                                                                                    
  SIRCCodedFrameLastWordLength = 0;
}

/**
  * @brief split state codes in to two word of the table .
  * @param state: the coded state to add to the coded frame table
  * @param freespace: the last coded frame table word free space
  * @retval  None
  */
static void SIRC_AddStateFragment(uint8_t state, uint8_t freespace)
{
  /* Shift the table to the left by one box */
  SIRC_Shift_Table(aSIRCFramePWForm);

  /*Test if the message to add is a high state code */
  if (state == SIRC_HIGH_STATE_CODE)
  {
    /* The message is a high state code */
    /* Add the first frame fragment to the First word of the table*/
    aSIRCFramePWForm[SIRCNbWord] = aSIRCFramePWForm[SIRCNbWord] << freespace ;
    aSIRCFramePWForm[SIRCNbWord] |= SIRC_HIGH_STATE_CODE >> (SIRC_HIGH_STATE_NB_SYMBOL - freespace);

    /* Add the Second frame fragment to the Second word of the table*/
    aSIRCFramePWForm[0] = aSIRCFramePWForm[0] << (SIRC_HIGH_STATE_NB_SYMBOL - freespace) ;
    aSIRCFramePWForm[0] |= ((SIRC_HIGH_STATE_CODE << (8 - SIRC_HIGH_STATE_NB_SYMBOL + freespace)) & 0xff) >> (8 - SIRC_HIGH_STATE_NB_SYMBOL + freespace);

    /* Increment the Last word of the coded frame counter*/
    SIRCCodedFrameLastWordLength = SIRC_HIGH_STATE_NB_SYMBOL - freespace ;
  }
  else
  {
    /* The message is a low state code */
    /* Add the first frame fragment to the First word of the table */
    aSIRCFramePWForm[SIRCNbWord] = aSIRCFramePWForm[SIRCNbWord] << freespace ;
    aSIRCFramePWForm[SIRCNbWord] |= SIRC_LOW_STATE_CODE >> (SIRC_LOW_STATE_NB_SYMBOL - freespace);

    /* Add the Second frame fragment to the Second word of the table */
    aSIRCFramePWForm[0] = aSIRCFramePWForm[0] << (SIRC_LOW_STATE_NB_SYMBOL - freespace) ;
    aSIRCFramePWForm[0] |= ((SIRC_LOW_STATE_CODE << (8 - SIRC_LOW_STATE_NB_SYMBOL + freespace)) & 0xff) >> (8 - SIRC_LOW_STATE_NB_SYMBOL + freespace);

    /* Increment the Last word of the coded frame counter */
    SIRCCodedFrameLastWordLength = SIRC_LOW_STATE_NB_SYMBOL - freespace ;
  }
}

/**
  * @brief Convert the SIRC message from binary to Pulse width modulation Format.
  *        Output is put into global variable.
  * @param binData: The SIRC message in binary format.
  * @param binDataLength: Number of bits
  * @retval None
  */
void SIRC_PulseWidthModulationConvert(uint32_t binData, uint8_t binDataLength)
{
  uint32_t dataframecount = 0;
  uint32_t bitformat = 0;
  uint32_t lastwordfreeespace = 0;

  for (dataframecount = 0; dataframecount < binDataLength; dataframecount++) //binDataLength = 12
  {
    /* Calculate last coded frame word free space */
    lastwordfreeespace = 32 - SIRCCodedFrameLastWordLength; //SIRCCodedFrameLastWordLength = 0
    /* Select one bit from the binary frame */
    bitformat = (uint32_t)((uint32_t)((((uint16_t)(binData)) >> dataframecount) & SIRC_BIT_FORMAT_MASK) << dataframecount); //SIRC_BIT_FORMAT_MASK = 1 (16bit)
    /* Test the bit format state */
    if (bitformat != 0)
    {
      /* Test if the last word of the frame enough space */
      if ((lastwordfreeespace) > (SIRC_HIGH_STATE_NB_SYMBOL - 1)) //SIRC_HIGH_STATE_NB_SYMBOL = 3
      {
        /*Shift left the the last coded frame word by State number of bits */
        aSIRCFramePWForm[0] = aSIRCFramePWForm[0] << SIRC_HIGH_STATE_NB_SYMBOL ;//SIRC_HIGH_STATE_NB_SYMBOL = 3

        /* Add the state to the last word of the coded frame table */
        aSIRCFramePWForm[0] |= SIRC_HIGH_STATE_CODE;//SIRC_HIGH_STATE_CODE  ((uint8_t)0x03) /* SIRC high level definition*/

        /*Increment the last coded frame word counter */
        SIRCCodedFrameLastWordLength = SIRCCodedFrameLastWordLength + SIRC_HIGH_STATE_NB_SYMBOL ;//SIRC_HIGH_STATE_NB_SYMBOL = 3
      }
      else
      {
        /* lastwordfreeespace = 2
        /* Split state code to two words */
        SIRC_AddStateFragment(SIRC_HIGH_STATE_CODE, lastwordfreeespace);//SIRC_HIGH_STATE_CODE  ((uint8_t)0x03) /* SIRC high level definition*/
      }
    }
    else
    {
      /*  bit format == 0 */
      /* Test if the last word of the frame enough space */
      if ((lastwordfreeespace) > 1)
      {
        /* enough space found */
        /* Shift left the the last coded frame word by State bits number */
        aSIRCFramePWForm[0] = aSIRCFramePWForm[0] << SIRC_LOW_STATE_NB_SYMBOL ;

        /* Shift left the the last coded frame word by State bits number */
        aSIRCFramePWForm[0] |= SIRC_LOW_STATE_CODE;

        /* Increment the Last word of the coded frame counter */
        SIRCCodedFrameLastWordLength = SIRCCodedFrameLastWordLength + SIRC_LOW_STATE_NB_SYMBOL ;
      }
      else
      {
        /* Split state code to two words */
        SIRC_AddStateFragment(SIRC_LOW_STATE_CODE, lastwordfreeespace);
      }
    }
  }
}

/**
  * @brief Split coded headers in to two word of the table.
  * @param headers: the coded headers to add to the coded frame table
  * @param freespace: the last coded frame table word free space
  * This parameter can be any of the @ref uint8_t.
  * @retval  None
  */
static void SIRC_AddHeadersFragment (uint8_t headers, uint8_t freespace)
{
  /* Shift the table to the left by one box */
  SIRC_Shift_Table(aSIRCFramePWForm);

  /* Shift left the the second coded frame word by headers second fragment bits number */
  aSIRCFramePWForm[SIRCNbWord] = aSIRCFramePWForm[SIRCNbWord] << freespace;

  /* Add the first headers fragment to the Second word of the table */
  aSIRCFramePWForm[SIRCNbWord] |= SIRC_HEADERS >> (SIRC_HEADERS_LENGTH - freespace);

  /* Shift left the the last coded frame word by headers bits number */
  aSIRCFramePWForm[0] = aSIRCFramePWForm[0] << (SIRC_HEADERS_LENGTH - freespace);

  /* Add the Second Headers fragment to the Second word of the table*/
  aSIRCFramePWForm[0] |= (((( SIRC_HEADERS) << (freespace + 3))) & 0xFF) >> ((freespace + 3));

  /* Increment the Last word of the coded frame counter */
  SIRCCodedFrameLastWordLength = SIRC_HEADERS_LENGTH - freespace;
}

/**
  * @brief Add coded headers to the coded frame table.
  * @param headers: This parameter can be any of the @ref uint8_t.
  * @retval None
  */
static void SIRC_AddHeaders(uint8_t headers)
{
  uint8_t lastwordfreespace = 0;

  /* Calculate last coded frame word free space */
  lastwordfreespace = 32 - SIRCCodedFrameLastWordLength;

  if (lastwordfreespace > 4)
  {
    /* Shift left the the last coded frame word by headers bits number */
    aSIRCFramePWForm[0] = aSIRCFramePWForm[0] << 5;

    /* Add the first headers fragment to the First word of the table */
    aSIRCFramePWForm[0] = aSIRCFramePWForm[0] | headers;

    /* Increment the Last word of the coded frame counter */
    SIRCCodedFrameLastWordLength = SIRCCodedFrameLastWordLength + SIRC_HEADERS_LENGTH;
  }
  else
  {
    SIRC_AddHeadersFragment(headers, lastwordfreespace);
  }
}
/**
  * @brief  Force new configuration to the output channel
  * @param  action: new configuration
  * @retval None
  */
void TIM_ForcedOC1Config(uint32_t action)
{
  uint32_t temporary = htim16.Instance->CCMR1;

  temporary &= ~TIM_CCMR1_OC1M;
  temporary |= action;
  htim16.Instance->CCMR1 = temporary;
}
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim == &htim16)
	{
		SIRC_Encode_SignalGenerate();
	}
}

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
