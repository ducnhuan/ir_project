/*
 * IR_Common.h
 *
 *  Created on: Aug 10, 2018
 *      Author: Admin
 */

#ifndef IR_COMMON_H_
#define IR_COMMON_H_

extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim16;
extern TIM_HandleTypeDef htim17;
extern UART_HandleTypeDef huart1;

/** @defgroup SIRC_Decode_State
  * @{
  */
enum
{
	INIT,
	IDLE,
	RECEIVING,
	RECEIVE_DONE
};

extern 	volatile uint8_t	SIRC_DecodeState;
extern	volatile uint16_t  	aSIRC_CaptureValue[100];
extern 	volatile uint16_t  	SIRC_CounterData;
extern	volatile uint8_t	SIRC_Repeat;
extern	volatile uint8_t	SIRC_DataReady;

void TIM_ForcedOC1Config(uint32_t action);

#endif /* IR_COMMON_H_ */
